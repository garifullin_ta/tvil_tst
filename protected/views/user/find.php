<?php
$this->breadcrumbs=array(
		'Users',
	);

$this->menu=array(
		array('label'=>'Create User', 'url'=>array('create')),
			array('label'=>'Manage User', 'url'=>array('admin')),
		);

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/user-find.css');
?>


<h1>Find user</h1>
<div class="find-user">
<h3>List</h3>
<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
	
	'name'=>'userList',
	'source'=>$list,
	'options'=>array(
		'minLength'=>'1',
		'select'=>"js:function(event, ui) {
			window.location.href='".$this->createUrl('user/view')."&id=' + ui.item.id;
		}",
	),
	'htmlOptions'=>array(
		'style'=>'height:20px;',
	),
));
?>

<h3>AJAX</h3>
<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
	
	'name'=>'userAJAX',
	'source'=>Yii::app()->createUrl('user/suggest'),
	'options'=>array(
		'minLength'=>'1',
		'select'=>"js:function(event, ui) {
			window.location.href='".$this->createUrl('user/view')."&id=' + ui.item.id;
		}",
	),
	'htmlOptions'=>array(
		'style'=>'height:20px;',
	),
));

?>
</div>
